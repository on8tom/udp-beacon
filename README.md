UDP Beacon Service
==================

this is a small C# program that can be runned as a console application
or a Windows Service.

it sends periodically an UPD datagram to a specified destination and port


config
------
in app.config you can change parameters.

| Setting     | type            | description                                  |
| ----------- | ----            | -------------------------------------------- |
| interval    | System.TimeSpan | the frequency of the beacon                  |
| payload     | String          | the message / payload of the UDP datagram    |
| destination | String          | the destination IP address                   |
| port        | Int             | UDP destination port                         |



use case
---------
one can send an UDP broadcast every 10 seconds.
When a Spanning Tree Protocol  Topology Change occurs.
every switch on the network flushes it's MAC addres table (FDB)
it does it by setting the age to the forward delay (default: 15s)

if you want that every switch in the network always knows where your server is
(to prevent unknown uniast translating to broadcast)
you can run this service


