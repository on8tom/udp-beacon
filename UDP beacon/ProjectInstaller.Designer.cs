﻿namespace UDP_beacon {
    partial class ProjectInstaller {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.UdpBeaconServiceProcessInstaller1 = new System.ServiceProcess.ServiceProcessInstaller();
            this.UdpBeaconServiceInstaller1 = new System.ServiceProcess.ServiceInstaller();
            // 
            // UdpBeaconServiceProcessInstaller1
            // 
            this.UdpBeaconServiceProcessInstaller1.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.UdpBeaconServiceProcessInstaller1.Password = null;
            this.UdpBeaconServiceProcessInstaller1.Username = null;
            // 
            // UdpBeaconServiceInstaller1
            // 
            this.UdpBeaconServiceInstaller1.DelayedAutoStart = true;
            this.UdpBeaconServiceInstaller1.Description = "sends a udp message to a host periodically";
            this.UdpBeaconServiceInstaller1.DisplayName = "UDP Beacon Service";
            this.UdpBeaconServiceInstaller1.ServiceName = "UdpBeaconService";
            this.UdpBeaconServiceInstaller1.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.UdpBeaconServiceProcessInstaller1,
            this.UdpBeaconServiceInstaller1});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller UdpBeaconServiceProcessInstaller1;
        private System.ServiceProcess.ServiceInstaller UdpBeaconServiceInstaller1;
    }
}