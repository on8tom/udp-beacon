﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace UDP_beacon {
    public partial class UdpBeaconService : ServiceBase {
        private UdpClient udpClient;
        private byte[] bytes;
        private  IPEndPoint ip;

        private Timer timer;

        public UdpBeaconService() {
            InitializeComponent();
        }

        protected override void OnStart(string[] args) {
            udpClient = new UdpClient();
            var ipaddress = IPAddress.Parse(Properties.Settings.Default.destination);
            ip = new IPEndPoint(ipaddress, Properties.Settings.Default.port);
            bytes = Encoding.UTF8.GetBytes(Properties.Settings.Default.payload);

            timer = new System.Timers.Timer();
            timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            timer.Interval = Properties.Settings.Default.interval.TotalMilliseconds;
            timer.Enabled = true;

        }

        protected override void OnStop() {
            timer.Stop();
            udpClient.Close();
        }

        public void start(string[] args) {
            OnStart(args);
        }

        public void stop() {
            OnStop();
        }

        private void OnTimedEvent(object source, ElapsedEventArgs e) {
            if (udpClient.Available > 0) {
                udpClient.Receive( ref ip);
            }
            udpClient.Send(bytes, bytes.Length, ip);
        }
    }
}
