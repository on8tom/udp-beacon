﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace UDP_beacon {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args) {
            if (Environment.UserInteractive) {
                Console.WriteLine("starting service");
                UdpBeaconService s = new UdpBeaconService();
                s.start(args);
                Console.WriteLine("Service Started.");
                Console.WriteLine("Press any key to stop...");
                Console.Read();
                s.stop();
            } else {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] { 
                    new UdpBeaconService() 
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
